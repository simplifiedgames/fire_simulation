mod collisions;
use collisions::{CollisionsPlugin, Collisions};

use bevy::{prelude::*, utils::HashMap};
use bevy_rapier2d::prelude::*;

#[derive(Component)]
struct Floor;

#[derive(Component)]
struct Fire{
    heat: f32,
}

impl Default for Fire {
    fn default() -> Self {
        Fire{
            heat: 0.0,
        }
    }
}

struct SimulationConstants {
    max_heat: f32,
    heat_multiplier: f32,
    heat_transfer_multiplier: f32,
    cool_multiplier: f32,
    speed_multiplier: f32,
    gravity: f32,
}

impl SimulationConstants {
    #[allow(dead_code)]
    fn lava() -> Self {
        SimulationConstants {
            max_heat: 5.0,
            heat_multiplier: 6.0,
            heat_transfer_multiplier: 1.7,
            cool_multiplier: 1.0,
            speed_multiplier: 500.0,
            gravity: 25.0,
        }
    }
    #[allow(dead_code)]
    fn balloon() -> Self {
        SimulationConstants {
            max_heat: 3.5,
            heat_multiplier: 9.0,
            heat_transfer_multiplier: 0.8,
            cool_multiplier: 0.6,
            speed_multiplier: 1400.0,
            gravity: 15.5,
        }
    }
    #[allow(dead_code)]
    fn fire() -> Self {
        SimulationConstants {
            max_heat: 6.0,
            heat_multiplier: 16.0,
            heat_transfer_multiplier: 4.0,
            cool_multiplier: 3.0,
            speed_multiplier: 1500.0,
            gravity: 20.0,
        }
    }
}

impl Default for SimulationConstants {
    fn default() -> Self {
        SimulationConstants::fire()
    }
}

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_plugin(RapierPhysicsPlugin::<NoUserData>::pixels_per_meter(100.0))
        .add_plugin(CollisionsPlugin)
        .insert_resource(ClearColor(Color::BLACK))
        .insert_resource(SimulationConstants::default())
        .add_startup_system(setup)
        .add_system(handle_fire)
        .add_system(handle_collision)
        .run();
}

fn handle_collision(mut fires: Query<(&Collisions, &mut Fire)>, floors: Query<With<Floor>>, time: Res<Time>, constants: Res<SimulationConstants>) {

    let mut new_fires: HashMap<usize, f32> = HashMap::new();

    for (i, (collisions, fire)) in fires.iter().enumerate() {
        for collision in collisions.entities() {
            let old_val = new_fires.get(&i).unwrap_or(&0.0);
            if let Ok(other_fire) = fires.get_component::<Fire>(collision) {
                if fire.heat > other_fire.heat {
                    let dif = fire.heat - other_fire.heat;
                    new_fires.insert(i, (fire.heat - dif * constants.heat_transfer_multiplier * time.delta_seconds()).max(*old_val));
                } else {
                    let dif = other_fire.heat - fire.heat;
                    new_fires.insert(i, (fire.heat + dif * constants.heat_transfer_multiplier * time.delta_seconds()).max(*old_val));
                }
            } else if let Ok(_) = floors.get(collision) {
                let old_val = fire.heat.max(*old_val);
                new_fires.insert(i, (old_val + constants.heat_multiplier * time.delta_seconds()).min(constants.max_heat));
            }
        }
    }

    for (i, (_, mut fire)) in fires.iter_mut().enumerate() {
        if let Some(new_heat) = new_fires.get(&i) {
            fire.heat = *new_heat;
        }
    }
}



fn handle_fire(mut objects: Query<(&mut Velocity, &mut Sprite, &mut Fire)>, time: Res<Time>, constants: Res<SimulationConstants>) {
    for (mut vel, mut sprite, mut fire) in objects.iter_mut() {
        if fire.heat > 0.0 {
            vel.linvel.y += fire.heat * time.delta_seconds() * constants.speed_multiplier;
        } else {
            vel.linvel.y -= time.delta_seconds() * constants.gravity;
        }
        fire.heat = (fire.heat - time.delta_seconds() * constants.cool_multiplier).max(0.0);
        sprite.color = Color::rgb(fire.heat * 0.88, fire.heat * 0.34, fire.heat * 0.13);
    }
}

fn setup(mut commands: Commands, windows: Res<Windows>) {
    
    let window = windows.get_primary().unwrap();
    let window_size = (window.width(), window.height());
    commands.spawn_bundle(Camera2dBundle::default());

    // Fire
    for i in -65..65 {
        for j in -45..45 {
            commands.spawn()
            .insert_bundle(SpriteBundle{
                transform: Transform { scale: Vec3::new(6.0, 6.0, 1.0), translation: Vec3::new((i * 8 + j * 2) as f32, -(j * 8) as f32, 0.0), ..default() },
                sprite: Sprite { color: Color::rgb(0.0, 0.0, 0.0), ..default() },
                ..default()
            })
            .insert(RigidBody::Dynamic)
            .insert(ActiveEvents::COLLISION_EVENTS)
            .insert(Sleeping::disabled())
            .insert(Ccd::enabled())
            .insert(Collider::ball(0.6))
            .insert(GravityScale(0.0))
            .insert(Velocity::default())
            .insert(Collisions::default())
            .insert(Fire::default());
        }
    }
    
    // Floor
    commands.spawn()
    .insert_bundle(SpriteBundle{
        transform: Transform { scale: Vec3::new(window_size.0, 5.0, 1.0), translation: Vec3::new(0.0, -window_size.1 / 2.0, 0.0), ..default() },
        sprite: Sprite { color: Color::rgb(0.0, 0.0, 0.0), ..default() },
        ..default()
    })
    .insert(Floor)
    .insert(RigidBody::Fixed)
    .insert(Collider::cuboid(window_size.0 * 0.1, 0.5));

    // Ceil
    commands.spawn()
        .insert_bundle(SpriteBundle{
            transform: Transform { scale: Vec3::new(window_size.0, 5.0, 1.0), translation: Vec3::new(0.0, window_size.1 / 2.0, 0.0), ..default() },
            sprite: Sprite { color: Color::rgb(0.0, 0.0, 0.0), ..default() },
            ..default()
        })
        .insert(RigidBody::Fixed)
        .insert(Collider::cuboid(window_size.0 * 0.1, 0.5));

    // Left wall
    commands.spawn()
        .insert_bundle(SpriteBundle{
            transform: Transform { scale: Vec3::new(5.0, window_size.1, 1.0), translation: Vec3::new(-window_size.0 / 2.0, 0.0, 0.0), ..default() },
            sprite: Sprite { color: Color::rgb(0.0, 0.0, 0.0), ..default() },
            ..default()
        })
        .insert(RigidBody::Fixed)
        .insert(Collider::cuboid(0.5, window_size.1 * 0.1));

    // Right wall
    commands.spawn()
    .insert_bundle(SpriteBundle{
        transform: Transform { scale: Vec3::new(5.0, window_size.1, 1.0), translation: Vec3::new(window_size.0 / 2.0, 0.0, 0.0), ..default() },
        sprite: Sprite { color: Color::rgb(0.0, 0.0, 0.0), ..default() },
        ..default()
    })
    .insert(RigidBody::Fixed)
    .insert(Collider::cuboid(0.5, window_size.1 * 0.1));

}