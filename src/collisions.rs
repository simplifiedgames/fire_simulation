use bevy::{prelude::*, utils::HashMap};
use bevy_rapier2d::{prelude::*, rapier::prelude::CollisionEventFlags};

#[derive(Component)]
pub struct Collisions(HashMap<Entity, CollisionEventFlags>);

impl Collisions {
    pub fn entities(&self) -> impl Iterator<Item = Entity> + '_ {
        self.0.keys().copied()
    }
}

impl Default for Collisions {
    fn default() -> Self {
        Collisions(HashMap::new())
    }
}

pub struct CollisionsPlugin;

impl Plugin for CollisionsPlugin {
    fn build(&self, app: &mut App) {
        app.add_system(handle_collisions);
    }
}

fn handle_collisions(mut collision_events: EventReader<CollisionEvent>, mut entities: Query<&mut Collisions>) {
    for event in collision_events.iter() {
        match event {
            CollisionEvent::Started(e1, e2, flags) => {
                let collision_1 = entities.get_mut(*e1);

                match collision_1 {
                    Ok(mut collisions) => {
                        collisions.0.insert(*e2, *flags);
                    },
                    Err(_) => {}
                }

                let collision_2 = entities.get_mut(*e2);

                match collision_2 {
                    Ok(mut collisions) => {
                        collisions.0.insert(*e1, *flags);
                    },
                    Err(_) => {}
                }
            },
            CollisionEvent::Stopped(e1, e2, _) => {
                let collision_1 = entities.get_mut(*e1);

                match collision_1 {
                    Ok(mut collisions) => {
                        collisions.0.remove(e2);
                    },
                    Err(_) => {}
                }

                let collision_2 = entities.get_mut(*e2);

                match collision_2 {
                    Ok(mut collisions) => {
                        collisions.0.remove(e1);
                    },
                    Err(_) => {}
                }
            },
        }
    }
}